package com.example.common.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Record<T> {

    private String topic;

    @Builder.Default
    private long timestamp = new Date().getTime();

    private CrudType type;

    private T data;

    public enum CrudType {
        READ,
        CREATE,
        UPDATE,
        DELETE,
    }
}
