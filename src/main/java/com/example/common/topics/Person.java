package com.example.common.topics;

import lombok.*;

import java.util.UUID;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Person {

    @Builder.Default
    @EqualsAndHashCode.Include
    private String id = UUID.randomUUID().toString();

    private String name;
}
